<?php if(isset($message)): ?>
    <div class="<?php echo $class; ?>"><?php echo $message; ?></div>
<?php endif; ?> 

<form action="" method="POST">

<table class="table table-striped table-flip-scroll cf">
    <thead class="cf">
        <tr>
            <th>Student ID</th>
            <th>Student Name</th>
            <th>Attendened Hours</th>
            <th>Percentage</th>
            <th>Attend</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($attendence_list as $list): ?>
        <tr>
            <td><?php echo $student->select('studentID', 'id', $list['studentID']); ?></td>
            <td><?php echo $student->select('name', 'id', $list['studentID']); ?></td>
            <td><?php echo $list['hours']; ?></td>
            <td>%<?php if($list['totalhours'] != 0){ echo $list['hours'] * 100 / $list['totalhours']; } else { echo '100';}?></td>
            <td>
                <div class="checkbox check-default">
                  <input id="checkbox-<?php echo $list['id']; ?>" type="checkbox" name="attend-<?php echo $list['studentID']; ?>" checked="">
                  <label for="checkbox-<?php echo $list['id']; ?>"></label>
                </div>
            </td>
            <input type="hidden" name="studentID-<?php echo $list['studentID']; ?>" value="<?php echo $list['studentID']; ?>">
        </tr>
    <?php endforeach; ?>
    </tbody>

</table>
    
    <br>

        <button type="submit" class="btn btn-primary btn-cons">Save</button>
        <a type="button" class="btn btn-danger btn-cons">Cancel</a>
    </form>

