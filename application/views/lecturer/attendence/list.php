<?php if(!$classes): ?>
    <div class="alert alert-danger">There is no classes at this moment.</div>
<?php else: ?>

<table class="table table-striped table-flip-scroll cf">
    <thead class="cf">
        <tr>
            <th>Subject Name</th>
            <th>Subject Code</th>
            <th>Class Start</th>
            <th>Class End</th>
            <th>Faculty</th>
            <th>Take Attendence</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($classes as $class): ?>
        <tr>
            <td><?php echo $class['subjectName']; ?></td>
            <td><?php echo $class['subjectCode']; ?></td>
            <td><?php echo $class['starttime']; ?></td>
            <td><?php echo $class['endtime']; ?></td>
            <td><?php echo $class['facultyName']; ?></td>
            <td><a href="<?php echo base_url(); ?>lecturer/attendence/take/<?php echo $class['id']; ?>">Take Attendence</a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php endif; ?>