<div id="container">
  <div class="row">
  <div class="col-md-6 col-sm-6">
      <div class="tiles-body">
        <div class="tiles-title"> Notes </div>
        <br>
        <?php if(!$notes): ?>
        <div class="notification-messages danger">
          <div class="message-wrapper">
            <div class="heading"> There is no notes at this moment. </div>
            <div class="description"> There is no notes at this moment. </div>
          </div>
        </div>
        <?php else: ?>
          <?php $class = array('info','danger','success','warning');$i = 0;?>
          <?php foreach($notes as $note): ?>
            <div class="notification-messages <?php echo $class[$i]; ?>">
              <div class="message-wrapper">
                <div class="heading"> <?php echo $note['title']; ?> </div>
                <div class="description"> <?php echo $note['text']; ?> </div>
              </div>
            </div>
          <?php $i++; if($i == 3) $i = 0; endforeach; ?>
      <?php endif; ?>
      </div>
  </div>

  <div class="col-md-6 col-sm-6">
  <div class="tiles-body">
        <div class="tiles-title"> Calendar </div>
    <div class="row" style="max-height:600px;">
            <div class="tiles-body">
              
              <div id='calendar'></div>
            </div>
        </div>
  </div>
</div>
</div>
<br>
<br>
<br>
<br>
<?php if(!$classes): ?>
    <div class="alert alert-danger">There is no classes for today.</div>
<?php else: ?>

<table class="table table-striped table-flip-scroll cf">
    <thead class="cf">
        <tr>
            <th>Subject Name</th>
            <th>Subject Code</th>
            <th>Class Start</th>
            <th>Class End</th>
            <th>Faculty</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($classes as $class): ?>
        <tr>
            <td><?php echo $class['subjectName']; ?></td>
            <td><?php echo $class['subjectCode']; ?></td>
            <td><?php echo $class['starttime']; ?></td>
            <td><?php echo $class['endtime']; ?></td>
            <td><?php echo $class['facultyName']; ?></td>
            <td><a href="<?php echo base_url(); ?>manager/classes/edit/<?php echo $class['id']; ?>">Edit</a></td>
            <td><a href="<?php echo base_url(); ?>manager/classes/delete/<?php echo $class['id']; ?>">Delete</a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php endif; ?>
</div>

