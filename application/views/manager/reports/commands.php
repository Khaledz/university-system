<h3><center>Summary of available commands</center></h3>

<table class="table no-more-tables">
    <thead>
        <tr>
            <th>Objective</th>
            <th>Data</th>
            <th>Command</th>
            <th>Example</th>
        </tr>
    </thead>
    <tbody>
        <tr>
           <td>Generate report about a student</td>
           <td>Student ID</td>
           <td>s</td>
           <td>s 110012345</td>
        </tr>
        <tr>
           <td>Generate report about a class</td>
           <td>Class ID</td>
           <td>c</td>
           <td>c ABC123</td>
        </tr>
    </tbody>
</table>
<?php if(isset($message)): ?>
    <div class="<?php echo $class; ?>"><?php echo $message; ?></div>
<?php endif; ?> 

<form action="" method="POST">
    <div class="form-group">
    <label class="form-label">Command:</label>
        <input type="text" name="command" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary btn-cons" name="classBtn">Generate</button>
    <a type="button" class="btn btn-danger btn-cons">Cancel</a>

</form>
