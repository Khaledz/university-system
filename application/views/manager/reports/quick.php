<div class="grid simple vertical green">
	<div class="grid-title no-border">
		<h4><span class="semi-bold">Specific Class</span></h4>
	</div>
	<div class="grid-body no-border">
		<div class="row-fluid ">
			<form action="" method="POST">
				<div class="form-group">
				    <label class="form-label">Classes:</label>
				    <div class="controls">
				      <select class="form-control" name="classID">
				      	<option>Class</option>
				      	<?php foreach($classes as $class): ?>
				      		<option value="<?php echo $class['id']; ?>"><?php echo $class['day']; ?> - <?php echo $class['starttime']; ?> to <?php echo $class['endtime']; ?> - <?php echo $subjects->select('name', 'id', $class['subjectID']); ?></option>
				      	<?php endforeach; ?>
				      </select>
				    </div>
				</div>

				<button type="submit" class="btn btn-primary btn-cons" name="classBtn">Generate</button>
				<a type="button" class="btn btn-danger btn-cons">Cancel</a>

			</form>
		</div>
	</div>
</div>

<br>

<div class="grid simple vertical green">
	<div class="grid-title no-border">
		<h4><span class="semi-bold">Specific Student</span></h4>
	</div>
	<div class="grid-body no-border">
		<div class="row-fluid ">
			<form action="" method="POST">
				<div class="form-group">
				    <label class="form-label">Students:</label>
				    <div class="controls">
				      <select class="form-control" name="studentID">
				      	<option>Student</option>
				      	<?php foreach($students as $student): ?>
				      		<option value="<?php echo $student['id']; ?>"><?php echo $student['studentID']; ?> - <?php echo $student['name']; ?></option>
				      	<?php endforeach; ?>
				      </select>
				    </div>
				</div>

				<button type="submit" class="btn btn-primary btn-cons" name="studentBtn">Generate</button>
				<a type="button" class="btn btn-danger btn-cons">Cancel</a>

			</form>
		</div>
	</div>
</div>