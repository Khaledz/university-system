<?php if(isset($message)): ?>
	<div class="<?php echo $class; ?>"><?php echo $message; ?></div>
<?php endif; ?>	

<form action="" method="POST">
	<div class="form-group">
      <div class="controls">
          <div class="alert alert-warning">Are you sure you want to delete this class?</div>
      </div>
    </div>

	<button type="submit" class="btn btn-primary btn-cons" name="delete">Yes</button>
	<a type="button" class="btn btn-danger btn-cons">Cancel</a>

</form>