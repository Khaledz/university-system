<?php if(isset($message)): ?>
	<div class="<?php echo $class; ?>"><?php echo $message; ?></div>
<?php endif; ?>	

<form action="" method="POST">
	<div class="form-group">
      <label class="form-label">Class Time Start At</label>
      <div class="controls">
          <input type="time" class="form-control" name="starttime">
      </div>
    </div>
    <div class="form-group">
      <label class="form-label">Class Time End At</label>
      <div class="controls">
          <input type="time" class="form-control" name="endtime">
      </div>
    </div>
	<div class="form-group">
	    <label class="form-label">Day</label>
	    <div class="controls">
	      <select class="form-control" name="day">
	        <option>Day</option>
	      	<option value="Monday">Monday</option>
	      	<option value="Tuesday">Tuesday</option>
	      	<option value="Wednesday">Wednesday</option>
	      	<option value="Thursday">Thursday</option>
	      	<option value="Friday">Friday</option>
	      </select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="form-label">Faculty</label>
	    <div class="controls">
	      <select class="form-control" name="facultyID" id="facultyID">
	      	<option value="0">Faculty</option>
	      	<?php foreach($faculties as $faculty): ?>
	      		<option value="<?php echo $faculty['id']; ?>"><?php echo $faculty['name']; ?></option>
	      	<?php endforeach; ?>
	      </select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="form-label">Lecturers</label>
	    <div class="controls">
	      <select class="form-control" name="lecturerID" id="lecturerID" disabled="">
	      	<option>Lecturer</option>
	      	
	      </select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="form-label">Subject</label>
	    <div class="controls">
	      <select class="form-control" name="subjectID">
	      	<option>Subject</option>
	      	<?php foreach($subjects as $subject): ?>
	      		<option value="<?php echo $subject['id']; ?>"><?php echo $subject['code']; ?> - <?php echo $subject['name']; ?></option>
	      	<?php endforeach; ?>
	      </select>
	    </div>
	</div>

	<button type="submit" class="btn btn-primary btn-cons">Save</button>
	<a type="button" class="btn btn-danger btn-cons">Cancel</a>

</form>

<script type="text/javascript">
$(function(){
	$('#facultyID').change(function(){

		var facultyID = $('#facultyID').val();
		
		$.ajax({
        type:"POST",
        url:"<?php echo base_url(); ?>manager/classes/listLecturersByAjax",
        data: {facultyID:facultyID},
        success:function (data) 
        {
        	if(data != '')
        	{
        		$('#lecturerID').removeAttr('disabled');
        		$('#lecturerID').html(data);
        	}
        	else 
        	{
        		$('#lecturerID').attr('disabled', 'disabled');
        	}
       	}
		});
	});
});
</script>