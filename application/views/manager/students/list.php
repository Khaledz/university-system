<?php if(!$students): ?>
    <div class="alert alert-danger">There is no students at this moment.</div>
<?php else: ?>

<table class="table table-striped table-flip-scroll cf">
    <thead class="cf">
        <tr>
            <th>ID#</th>
            <th>Name</th>
            <th>Faculty</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($students as $student): ?>
        <tr>
            <td><?php echo $student['id']; ?></td>
            <td><?php echo $student['name']; ?></td>
            <td><?php echo $faculty->select('name', 'id', $student['facultyID']); ?></td>
            <td><a href="<?php echo base_url(); ?>manager/students/edit/<?php echo $student['id']; ?>">Edit</a></td>
            <td><a href="<?php echo base_url(); ?>manager/students/delete/<?php echo $student['id']; ?>">Delete</a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php endif; ?>