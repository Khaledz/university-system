<div class="row" style="max-height:600px;">
          <div class="col-md-12 tiles white no-padding">
            <div class="tiles-body">
              <div class="full-calender-header">
                <div class="pull-left">
                  <div class="btn-group ">
                    <button class="btn btn-success" id="calender-prev"><i class="fa fa-angle-left"></i></button>
                    <button class="btn btn-success" id="calender-next"><i class="fa fa-angle-right"></i></button>
                  </div>
                </div>
                <div class="pull-right">
                  <div data-toggle="buttons-radio" class="btn-group">
                    <button class="btn btn-primary active" type="button" id="change-view-month">month</button>
                    <button class="btn btn-primary " type="button" id="change-view-week">week</button>
                    <button class="btn btn-primary" type="button" id="change-view-day">day</button>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div id='calendar'></div>
            </div>
          </div>
        </div>
      
