<?php if(isset($message)): ?>
	<div class="<?php echo $class; ?>"><?php echo $message; ?></div>
<?php endif; ?>	

<form action="" method="POST">
<div class="alert alert-info">This feature enable you to back up the database easily. Press backup to start.</div>
	<input type="hidden" class="form-control" name="studentID">

  <button type="submit" class="btn btn-primary btn-cons">Backup</button>
	<a type="button" class="btn btn-danger btn-cons">Cancel</a>

</form>