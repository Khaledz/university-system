<?php if(isset($message)): ?>
	<div class="<?php echo $class; ?>"><?php echo $message; ?></div>
<?php endif; ?>	

<form action="" method="POST" enctype="multipart/form-data">
<div class="alert alert-info">This feature enable you to restore the database easily. please upload .sql file.</div>
	<input type="file" class="form-control" name="userfile">
<br>
  <button type="submit" class="btn btn-primary btn-cons">restore</button>
	<a type="button" class="btn btn-danger btn-cons">Cancel</a>

</form>