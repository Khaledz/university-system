<?php if(isset($message)): ?>
	<div class="<?php echo $class; ?>"><?php echo $message; ?></div>
<?php endif; ?>	

<form action="" method="POST">
	<div class="form-group">
	    <label class="form-label">Title</label>
	    <div class="controls">
	      <input name="title" class="form-control" value="<?php echo $note['title']; ?>" />
	    </div>
	</div>
	<div class="form-group">
	    <label class="form-label">Note</label>
	    <div class="controls">
	      <textarea name="text" class="form-control" rows="6"><?php echo $note['text']; ?></textarea>
	    </div>
	</div>

	<button type="submit" class="btn btn-primary btn-cons">Save</button>
	<a type="button" class="btn btn-danger btn-cons">Cancel</a>

</form>