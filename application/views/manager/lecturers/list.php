<?php if(!$lecturers): ?>
    <div class="alert alert-danger">There is no lecturers at this moment.</div>
<?php else: ?>

<table class="table table-striped table-flip-scroll cf">
    <thead class="cf">
        <tr>
            <th>ID#</th>
            <th>Name</th>
            <th>Faculty</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($lecturers as $lecturer): ?>
        <tr>
            <td><?php echo $lecturer['id']; ?></td>
            <td><?php echo $lecturer['name']; ?></td>
            <td><?php echo $faculty->select('name', 'id', $lecturer['facultyID']); ?></td>
            <td><a href="<?php echo base_url(); ?>manager/lecturers/edit/<?php echo $lecturer['id']; ?>">Edit</a></td>
            <td><a href="<?php echo base_url(); ?>manager/lecturers/delete/<?php echo $lecturer['id']; ?>">Delete</a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php endif; ?>