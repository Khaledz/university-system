<?php if(isset($message)): ?>
	<div class="<?php echo $class; ?>"><?php echo $message; ?></div>
<?php endif; ?>	

<form action="" method="POST">
	<div class="form-group">
      <label class="form-label">Name</label>
      <div class="controls">
          <input type="text" class="form-control" name="name" value="<?php echo $data['name']; ?>">
      </div>
    </div>
	<div class="form-group">
      <label class="form-label">Username</label>
      <div class="controls">
          <input type="text" class="form-control" name="username" value="<?php echo $data['username']; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="form-label">Password (optional)</label>
      <div class="controls">
          <input type="password" class="form-control" name="userpass">
      </div>
    </div>
	<div class="form-group">
	    <label class="form-label">Faculty</label>
	    <div class="controls">
	      <select class="form-control" name="facultyID">
	      	<option>Faculty</option>
	      	<?php foreach($faculties as $faculty): ?>
	      		<?php if($data['facultyID'] == $faculty['id']): ?>
	      			<option value="<?php echo $faculty['id']; ?>" selected><?php echo $faculty['name']; ?></option>
	      			<?php continue; ?>
	      		<?php endif; ?>
	      		<option value="<?php echo $faculty['id']; ?>"><?php echo $faculty['name']; ?></option>
	      	<?php endforeach; ?>
	      </select>
	    </div>
	</div>

	<button type="submit" class="btn btn-primary btn-cons">Save</button>
	<a type="button" class="btn btn-danger btn-cons">Cancel</a>

</form>