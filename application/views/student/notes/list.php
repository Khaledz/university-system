<?php if(!$notes): ?>
    <div class="alert alert-danger">There is no notes at this moment.</div>
<?php else: ?>

<table class="table table-striped table-flip-scroll cf">
    <thead class="cf">
        <tr>
            <th>Title</th>
            <th>Text</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($notes as $note): ?>
        <tr>
            <td><?php echo $note['title']; ?></td>
            <td><?php echo $note['text']; ?></td>
            <td><a href="<?php echo base_url(); ?>manager/notes/edit/<?php echo $note['id']; ?>">Edit</a></td>
            <td><a href="<?php echo base_url(); ?>manager/notes/delete/<?php echo $note['id']; ?>">Delete</a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php endif; ?>