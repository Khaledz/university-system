<?php if(isset($message)): ?>
	<div class="<?php echo $class; ?>"><?php echo $message; ?></div>
<?php endif; ?>	

<form action="" method="POST">
	<div class="form-group">
      <label class="form-label">Event Start At</label>
      <div class="controls">
          <input type="date" class="form-control" name="start">
      </div>
    </div>
    <div class="form-group">
      <label class="form-label">Event Time End At</label>
      <div class="controls">
          <input type="date" class="form-control" name="end">
      </div>
    </div>
	<div class="form-group">
	    <label class="form-label">event</label>
	    <div class="controls">
	      <textarea name="event" class="form-control" rows="6"></textarea>
	    </div>
	</div>
	<?php $managerID = $this->session->userdata('managerdata'); ?>
	<input type="hidden" name="managerID" value="<?php echo $managerID['id']; ?>">

	<button type="submit" class="btn btn-primary btn-cons">Save</button>
	<a type="button" class="btn btn-danger btn-cons">Cancel</a>

</form>