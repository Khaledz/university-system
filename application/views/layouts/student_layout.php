
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title><?php echo isset($title) ? 'Limkokwing Attendence System - '.$title.'' : 'Limkokwing Attendence System' ; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />

<link href="<?php echo base_url(); ?>assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url(); ?>assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen"/>
<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="<?php echo base_url(); ?>assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" media="screen"/>
<!-- END CORE CSS FRAMEWORK -->

<!-- BEGIN CSS TEMPLATE -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/css/custom-icon-set.css" rel="stylesheet" type="text/css"/>
<!-- END CSS TEMPLATE -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script> 
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse "> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
	<div class="header-seperation"> 
		<ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
		 <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>		 
		</ul>
      <!-- BEGIN LOGO -->	
      <a href="index.html"><img src="<?php echo base_url(); ?>assets/img/logo.png" class="logo" alt=""  data-src="<?php echo base_url(); ?>assets/img/logo.png" data-src-retina="<?php echo base_url(); ?>assets/img/logo2x.png" width="106" height="21"/></a>
      <!-- END LOGO --> 
      </div>
      <!-- END RESPONSIVE MENU TOGGLER --> 
      <div class="header-quick-nav" > 
      
	 <!-- BEGIN CHAT TOGGLER -->
      <div class="pull-right"> 
		
		 <?php if($this->session->userdata('lecturerdata')): ?>
      <ul class="nav quick-section ">
      <li class="quicklinks"> 
        <a class="dropdown-toggle  pull-right " href="#">           
          Hi, <?php $session = $this->session->userdata('lecturerdata'); echo $session['username']; ?>
        </a>
      </li> 
      <li class="quicklinks"> <span class="h-seperate"></span></li> 
      <li class="quicklinks"> 
        <a class="dropdown-toggle  pull-right " href="<?php echo base_url(); ?>lecturer/session/logout">           
          Signout
        </a>
      </li> 
    </ul>
     
     <?php endif; ?>
      </div>
	   <!-- END CHAT TOGGLER -->
      </div> 
      <!-- END TOP NAVIGATION MENU --> 
   
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>

<!-- END HEADER --> 

<!-- BEGIN CONTAINER -->
<div class="page-container row"> 
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar" id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper" id="main-menu-wrapper">
      
     <!-- BEGIN SIDEBAR MENU -->
      <ul>
      <p></p>
        <li class="start"> <a href="<?php echo base_url(); ?>student/dashboard"> <i class="icon-custom-home"></i> <span class="title">Dashboard</span> <span class="selected"></span></a> 
		</li>
       
        <li class=""> <a href="javascript:;"> <i class="icon-custom-ui"></i> <span class="title">Calendar</span> <span class="arrow "></span> </a>
          <ul class="sub-menu">
            <li > <a href="<?php echo base_url(); ?>student/events/"> View Events </a> </li>
            <li > <a href="<?php echo base_url(); ?>student/events/create"> Add Event </a> </li>
          </ul>
        </li>
        <li class=""> <a href="javascript:;"> <i class="icon-custom-ui"></i> <span class="title">Notes</span> <span class="arrow "></span> </a>
          <ul class="sub-menu">
            <li > <a href="<?php echo base_url(); ?>student/notes/"> List Notes </a> </li>
            <li > <a href="<?php echo base_url(); ?>student/notes/create"> Create Note </a> </li>
          </ul>
        </li>

      </ul>
      
      <div class="clearfix"></div>
      <!-- END SIDEBAR MENU -->
    </div>
  </div>
  
  <!-- END SIDEBAR --> 


  </div>
  </div>
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content"> 
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="clearfix"></div>
    <div class="content">  
    <ul class="breadcrumb">
        <li>
          <p>YOU ARE HERE</p>
        </li>
        <li><a href="#" class="active"><?php echo $title; ?></a> </li>
      </ul>

      <div class="row">
        <div class="col-md-15">
          <div class="grid simple">
            <div class="grid-title no-border">
              <h4><?php echo $title; ?></h4>
              <br>
            <div class="grid-body no-border">
              <div class="row">
                <div class="col-md-15">
                    <?php 
						if(isset($view) && isset($data))
						{
							$this->load->view($view, $data);
						}
						elseif(isset($view) && !isset($data))
						{
							$this->load->view($view);
						}
						else
						{
							echo 'Cannot load content';
						}		
					?>
                </div>
              </div>
              </div>
            </div>
        </div>
      </div>   
      
    </div>
  </div>
 </div>
<!-- END CONTAINER --> 

<!-- BEGIN CORE JS FRAMEWORK--> 
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/breakpoints.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script> 
<!-- END CORE JS FRAMEWORK --> 
<!-- BEGIN PAGE LEVEL JS --> 	
<script src="<?php echo base_url(); ?>assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script> 	
<script src="<?php echo base_url(); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
<script src="<?php echo base_url(); ?>assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- END PAGE LEVEL PLUGINS --> 	

<!-- BEGIN CORE TEMPLATE JS --> 
<script src="<?php echo base_url(); ?>assets/js/core.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>assets/js/chat.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>assets/js/demo.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>assets/js/student/calender.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS --> 
</body>
</html>