<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Manager extends CI_Controller
{
	protected $layout;

    protected $managerID;

    public function __construct()
    {
       parent::__construct();

        $this->layout = 'layouts/manager_layout';

        date_default_timezone_set("Asia/Kuala_Lumpur");

        if(!$this->session->userdata('managerdata'))
        {
        	redirect('manager/session/login');
        }
        else
        {
            $session = $this->session->userdata('managerdata');
            $this->managerID = $session['id'];
        }
    }
} 