<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Lecturer extends CI_Controller
{
	protected $layout;

    protected $lecturerID;

    public function __construct()
    {
       parent::__construct();

        $this->layout = 'layouts/lecturer_layout';

        date_default_timezone_set("Asia/Kuala_Lumpur");

        if(!$this->session->userdata('lecturerdata'))
        {
        	redirect('lecturer/session/login');
        }
        else
        {
            $session = $this->session->userdata('lecturerdata');
            $this->lecturerID = $session['id'];
        }
    }
} 