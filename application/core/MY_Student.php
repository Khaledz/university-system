<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Student extends CI_Controller
{
	protected $layout;

    protected $studentID;

    public function __construct()
    {
       parent::__construct();

        $this->layout = 'layouts/student_layout';

        date_default_timezone_set("Asia/Kuala_Lumpur");

        if(!$this->session->userdata('studentdata'))
        {
        	redirect('student/session/login');
        }
        else
        {
            $session = $this->session->userdata('studentdata');
            $this->studentID = $session['id'];
        }
    }
} 