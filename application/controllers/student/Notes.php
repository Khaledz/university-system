<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notes extends MY_Student
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('note_model');
    }

    public function index()
    {
        $data = array(
            'title' => 'List Notes',
            'view' => 'student/notes/list',
            'notes' => $this->note_model->getRowsByStudentID($this->studentID)
        );

        $this->load->view($this->layout, $data);
    }

    public function create()
    {
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('text', 'text', 'required');

        $data = array(
            'title' => 'Create a note',
            'view' => 'student/notes/create'
        );

        $note = array(
            'title' => $this->input->post('title'),
            'text' => $this->input->post('text'),
            'studentID' => $this->studentID
        );

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->note_model->save($note);

                $data['message'] = 'The note has been added successfully.';
                $data['class']   = 'alert alert-success';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

        $this->load->view($this->layout, $data);
    }

    public function edit($id)
    {
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('text', 'text', 'required');

        $data = array(
            'title' => 'Edit a note',
            'view' => 'student/notes/edit',
            'note' => $this->note_model->getRow($id)
        );

        $note = array(
            'id' => $id,
            'title' => $this->input->post('title'),
            'text' => $this->input->post('text'),
            'studentID' => $this->studentID,
        );

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->note_model->update($note);

                $data['message'] = 'The note has been edited successfully.';
                $data['class']   = 'alert alert-success';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

        $this->load->view($this->layout, $data);
    }

    public function delete($id)
    {
        $data = array(
            'title' => 'Delete a note',
            'view' => 'student/notes/delete',
        );

        if($this->input->post())
        {
            $this->note_model->delete($id);

            $data['message'] = 'The note has been deleted successfully.';
            $data['class']   = 'alert alert-success';
            
            redirect('student/notes');
        }

        $this->load->view($this->layout, $data);
    }
}
