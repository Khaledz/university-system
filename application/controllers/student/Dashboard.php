<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Student
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('note_model');
        $this->load->model('class_model');
        $this->load->model('event_model');
    }

    public function index()
    {
        $timestamp = strtotime(date('Y-m-d'));

        $day = date('l', $timestamp);

        $data = array(
            'title' => 'Dashboard',
            'view' => 'student/dashboard/index',
            'notes' => $this->note_model->getRowsByStudentID($this->studentID),
            'classes' => $this->class_model->getClassesByStudentSession($this->studentID, $day),
            'events' => $this->event_model->getEventsByStudentID($this->studentID)
        );

        $this->load->view($this->layout, $data);
    }
}
