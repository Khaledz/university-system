<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Lecturer
{
	function __construct()
	{
		parent::__construct();

        $this->load->model('class_model');
        $this->load->model('subject_model');
        $this->load->model('student_model');
        $this->load->model('attendence_model');
	}

	public function index(){}

	public function quick()
	{
		$data = array(
      'title' => 'Create a class',
      'view' => 'manager/reports/quick',
        'classes' => $this->class_model->getRows(),
        'students' => $this->student_model->getRows(),
        'subjects' => $this->subject_model,
        'subject' => $this->subject_model->getRows()
    );

        if(gettype($this->input->post('classBtn')) == 'string')
        {
            $this->form_validation->set_rules('classID', 'classID', 'required');

            if($this->form_validation->run())
            {
                $class   = array('class' => $this->class_model->getRow($this->input->post('classID')));
                $subject = array('subject' => $this->subject_model->getRow($class['class']['subjectID']));
                $student = array('students' => $this->attendence_model->getAllStudentsByClassID($this->input->post('classID')));
                $information = array_merge($subject, $class, $student);

                $this->quick_template_class($information);
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

        if(gettype($this->input->post('studentBtn')) == 'string')
        {
            $this->form_validation->set_rules('studentID', 'studentID', 'required');

            if($this->form_validation->run())
            {
                $class   = array('class' => $this->class_model->getRow($this->input->post('studentID')));
                $subject = array('subject' => $this->subject_model->getRow($class['class']['subjectID']));
                $student = array('students' => $this->attendence_model->getStudentInformation($this->input->post('studentID')));
                $information = array_merge($subject, $class, $student);

                $this->quick_template_student($information);
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view($this->layout, $data);
	}

    function quick_template_class($data)
    {
        $template = '<div class="row">
      <div class="col-md-11">
        <div class="grid simple">
          <div class="grid-body no-border invoice-body"> <br>
            <div align="center"> <img src="assets/logo.jpg" height="150" width="320">
            </div>
            <div align="center">
              <h3 align="center">Class '.$data['subject']['code']. ' - ' .$data['subject']['name'].'</h3>
            </div>
            <div class="row">
              <div class="col-md-9">
                <h4 class="semi-bold">'.$data['class']['day'].' from '.$data['class']['starttime']. ' to '. $data['class']['endtime'].'</h4>
               </div>
            </div>
            <table class="table">
              <thead>
                <tr>
                  <th>studentID</th>
                  <th style="width:250px">Name</th>
                  <th style="width:250px">Attended Hours</th>
                  <th style="width:250px">Percentage</th>
                </tr>
              </thead>
              <tbody>';
              foreach($data['students'] as $student){
                $template .= '<tr>
                  <td >'.$student['studentID'].'</td>
                  <td align="center">'.$student['name'].'</td>
                  <td align="center">'.$student['hours'].'</td>
                  <td align="center">%'.$student['hours'] * 100 / $student['totalhours'].'</td>
                </tr>';
              }
              $template .= '</tbody>
            </table>
            <br>
            <br>
          </div>
        </div>
      </div>
</div>';

        $this->load->helper('file');
        $this->load->library('pdfobj');
        $pdf = $this->pdfobj->load();
        $pdf->setFooter('{PAGENO}');
        $pdf->WriteHTML($template); // write the HTML into the PDF        
        $pdf->Output();

    }

    function quick_template_student($data)
    {
        $template = '<div class="row">
      <div class="col-md-11">
        <div class="grid simple">
          <div class="grid-body no-border invoice-body"> <br>
            <div align="center"> <img src="assets/logo.jpg" height="150" width="320">
            </div>
            <div align="center">
              <h3 align="center">Class '.$data['subject']['code']. ' - ' .$data['subject']['name'].'</h3>
            </div>
            <table class="table">
              <thead>
                <tr>
                  <th>studentID</th>
                  <th style="width:250px">Name</th>
                  <th style="width:250px">Class</th>
                  <th style="width:250px">Percentage</th>
                </tr>
              </thead>
              <tbody>';
              foreach($data['students'] as $student){
                $template .= '<tr>
                  <td >'.$student['studentID'].'</td>
                  <td align="center">'.$student['name'].'</td>
                  <td align="center">'.$this->subject_model->select('name','id',$this->class_model->select('subjectID','id',$student['classID'])).'</td>
                  <td align="center">%'.$student['hours'] * 100 / $student['totalhours'].'</td>
                </tr>';
              }
              $template .= '</tbody>
            </table>
            <br>
            <br>
          </div>
        </div>
      </div>
</div>';

        $this->load->helper('file');
        $this->load->library('pdfobj');
        $pdf = $this->pdfobj->load();
        $pdf->setFooter('{PAGENO}');
        $pdf->WriteHTML($template); // write the HTML into the PDF        
        $pdf->Output();

    }

    public function commands()
    {
        $data = array(
          'title' => 'Create a class',
          'view' => 'manager/reports/commands',
        );

        $this->form_validation->set_rules('command', 'command', 'required');

        if($this->input->post())
        {
          if($this->form_validation->run())
          {
            if(substr($this->input->post('command'), 0, 1) == 's')
            {
              $studentID = $this->student_model->select('id','studentID', substr($this->input->post('command'), 2));

              if(!$studentID)
              {
                $data['message'] = 'Student not found!';
                $data['class']   = 'alert alert-danger';
              }
              else
              {
                  $class   = array('class' => $this->class_model->getRow($studentID));
                  $subject = array('subject' => $this->subject_model->getRow($class['class']['subjectID']));
                  $student = array('students' => $this->attendence_model->getStudentInformation($studentID));
                  $information = array_merge($subject, $class, $student);
                  $information = array_merge($subject, $class, $student);

                $this->quick_template_student($information);
              }
            }
            else if(substr($this->input->post('command'), 0, 1) == 'c')
            {

              $subjectID = $this->subject_model->select('id','code', substr($this->input->post('command'), 2));
              $classID   = $this->class_model->select('id','subjectID', $subjectID);

              if(!$classID)
              {
                $data['message'] = 'Class not found!';
                $data['class']   = 'alert alert-danger';
              }
              else
              {


                $class   = array('class' => $this->class_model->getRow($classID));
                $subject = array('subject' => $this->subject_model->getRow($class['class']['subjectID']));
                $student = array('students' => $this->attendence_model->getAllStudentsByClassID($classID));
                $information = array_merge($subject, $class, $student);

                $this->quick_template_class($information);
              }
            }
              
          }
          else
          {
              $data['message'] = validation_errors();
              $data['class']   = 'alert alert-danger';
          }
        }

        $this->load->view($this->layout, $data);
    }


}
