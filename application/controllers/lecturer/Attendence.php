<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendence extends MY_Lecturer
{
	function __construct()
	{
		parent::__construct();

        $this->load->model('class_model');
        $this->load->model('attendence_model');
        $this->load->model('student_model');
	}

	public function index()
	{
		$data = array(
	        'title' => 'Take Attendence',
	        'view' => 'lecturer/attendence/list',
            'classes' => $this->class_model->getClassesByLectrerID($this->lecturerID)
        );

		$this->load->view($this->layout, $data);
	}

    public function take($id)
    {
        $data = array(
            'title' => 'Take Attendence',
            'view' => 'lecturer/attendence/take',
            'attendence_list' => $this->attendence_model->getStudentsByClassID($id),
            'student' => $this->student_model,
        );

        $hours = $this->class_model->select('hours', 'id', $id);

        if($this->input->post())
        {
            foreach($data['attendence_list'] as $list)
            {
                $student['studentID']  = $list['studentID'];
                $student['totalhours'] = $this->attendence_model->multiSelect('totalhours', 'studentID', $list['studentID'], 'classID', $id) + $hours;
                $student['classID'] = $id;
                
                if($this->input->post('attend-'.$list['studentID'].''))
                {
                    $student['hours'] = $this->attendence_model->multiSelect('hours', 'studentID', $list['studentID'], 'classID', $id) + $hours;
                    $this->attendence_model->update($student);
                }
                else
                {
                    $student['hours'] = $this->attendence_model->multiSelect('hours', 'studentID', $list['studentID'], 'classID', $id);
                    $this->attendence_model->update($student);
                }
            }

            $data['class'] = 'alert alert-success';
            $data['message'] = 'The list has been updated successfully.';
        }

        $this->load->view($this->layout, $data);
    }
}
