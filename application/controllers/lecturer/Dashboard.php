<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Lecturer
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('note_model');
        $this->load->model('class_model');
        $this->load->model('event_model');
    }

    public function index()
    {
        $timestamp = strtotime(date('Y-m-d'));

        $day = date('l', $timestamp);

        $data = array(
            'title' => 'Dashboard',
            'view' => 'manager/dashboard/index',
            'notes' => $this->note_model->getRowsByLecturerID($this->lecturerID),
            'classes' => $this->class_model->getClassesByLecturerSession($this->lecturerID, $day),
            'events' => $this->event_model->getEventsByLecturerID($this->lecturerID)
        );

        $this->load->view($this->layout, $data);
    }
}
