<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Session extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

        $this->load->model('lecturer_model');
	}

	public function index(){}

	public function login()
	{
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('userpass', 'userpass', 'required');

		$data = array(
	        'title' => 'Log In',
	        'view' => 'manager/sessions/start',
        );

        $lecturer = array(
        	'username' => $this->input->post('username'),
        	'userpass' => $this->input->post('userpass')
        );

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                if($this->lecturer_model->login($lecturer['username'], $lecturer['userpass']))
                {
                    $lecturer['id'] = $this->lecturer_model->select('id', 'username', $lecturer['username']);
                    $this->session->set_userdata('lecturerdata', $lecturer);
                    redirect('lecturer/dashboard');
                }
                else
                {
                    $data['message'] = 'The information is not correct.';
                    $data['class']   = 'alert alert-danger';
                }
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view('manager/sessions/start', $data);
	}

	public function logout()
	{
        $data = array(
            'title' => 'Log Out',
            'view' => 'manager/sessions/logout',
        );

		if($this->session->userdata('lecturerdata'))
        {
            $this->session->unset_userdata('lecturerdata');

            $data['message'] = 'You have logged ou successfully.';
            $data['class'] = 'alert alert-success';
        }

		$this->load->view('layouts/lecturer_layout', $data);
	}
}
