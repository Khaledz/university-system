<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lecturers extends MY_Manager
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('lecturer_model');
		$this->load->model('faculty_model');
	}

	public function index()
	{
		$data = array(
	        'title' => 'List Lecturers',
	        'view' => 'manager/lecturers/list',
	        'lecturers' => $this->lecturer_model->getRows(),
            'faculty' => $this->faculty_model
        );

		$this->load->view($this->layout, $data);
	}

	public function create()
	{
        $this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('userpass', 'userpass', 'required');
		$this->form_validation->set_rules('facultyID', 'facultyID', 'required|integer');

		$data = array(
	        'title' => 'Create a lecturer',
	        'view' => 'manager/lecturers/create',
	        'faculties' => $this->faculty_model->getRows(),
        );

        $lecturer = array(
        	'username' => $this->input->post('username'),
        	'userpass' => md5($this->input->post('userpass')),
            'name' => $this->input->post('name'),
        	'facultyID' => $this->input->post('facultyID')
        );

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->lecturer_model->save($lecturer);

                $data['message'] = 'The lecturer has been added successfully.';
                $data['class']   = 'alert alert-success';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view($this->layout, $data);
	}

	public function edit($id)
	{
        $this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('facultyID', 'facultyID', 'required|integer');

		$data = array(
	        'title' => 'Edit a lecturer',
	        'view' => 'manager/lecturers/edit',
	        'faculties' => $this->faculty_model->getRows(),
	        'data' => $this->lecturer_model->getRow($id)
        );

        $lecturer = array(
        	'id' => $id,
        	'username' => $this->input->post('username'),
            'userpass' => md5($this->input->post('userpass')),
            'facultyID' => $this->input->post('facultyID'),
            'name' => $this->input->post('name'),
        );

        if($lecturer['userpass'] == 'd41d8cd98f00b204e9800998ecf8427e')
        {
            $lecturer['userpass'] = $data['data']['userpass'];
        } 

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->lecturer_model->update($lecturer);

                $data['message'] = 'The lecturer has been edited successfully.';
                $data['class']   = 'alert alert-success';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view($this->layout, $data);
	}

	public function delete($id)
	{
		$data = array(
	        'title' => 'Delete a lecturer',
	        'view' => 'manager/lecturers/delete',
        );

		if($this->input->post())
        {
            $this->lecturer_model->delete($id);

            $data['message'] = 'The lecturer has been deleted successfully.';
            $data['class']   = 'alert alert-success';
            
            redirect('manager/lecturers');
        }

        $this->load->view($this->layout, $data);
	}
}
