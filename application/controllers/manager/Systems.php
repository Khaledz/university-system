<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Systems extends MY_Manager
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('system_model');

        $this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		
	}

	public function backup()
	{
		$data = array(
	        'title' => 'Backup a database',
	        'view' => 'manager/system/backup'
        );

        if($this->input->post())
        {
            $this->backup_tables('localhost','root','','abdulqader',$tables = '*');

            $data['class'] = 'alert alert-success';
            $data['message'] = 'You have imported the database successfully. <a href="'.base_url().'assets/db/db-backup-'.date('h-i-s').'.sql">Click Here To Download</a>';
        }

		$this->load->view($this->layout, $data);
	}

    function backup_tables($host,$user,$pass,$name,$tables = '*')
    {
    
        $link = @mysql_connect($host,$user,$pass);
        mysql_select_db($name,$link);
        
        //get all of the tables
        if($tables == '*')
        {
            $tables = array();
            $result = mysql_query('SHOW TABLES');
            while($row = mysql_fetch_row($result))
            {
                $tables[] = $row[0];
            }
        }
        else
        {
            $tables = is_array($tables) ? $tables : explode(',',$tables);
        }
        
        //cycle through
        foreach($tables as $table)
        {
            $result = @mysql_query('SELECT * FROM '.$table);
            $num_fields = mysql_num_fields($result);
            
            $row2 = @mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
            @$return.= "\n\n".$row2[1].";\n\n";
            
            for ($i = 0; $i < $num_fields; $i++) 
            {
                while($row = mysql_fetch_row($result))
                {
                    $return.= 'INSERT INTO '.$table.' VALUES(';
                    for($j=0; $j<$num_fields; $j++) 
                    {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = @ereg_replace("\n","\\n",$row[$j]);
                        if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                        if ($j<($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n\n";
        }
        
        //save file
        $handle = fopen('assets/db/db-backup-'.date('h-i-s').'.sql','w+');
        fwrite($handle,$return);
        fclose($handle);
    }

    function restore()
    {
        $data = array(
            'title' => 'Backup a database',
            'view' => 'manager/system/restore'
        );

        $sql_file = @$_FILES["userfile"];
        $sql_file = @$sql_file['name'];

        $allLines = @file('assets/db/'.$sql_file);

        $mysqli = new mysqli('localhost', 'root', '', 'abdulqader');
        $mysqli->query('SET foreign_key_checks = 0');
        @preg_match_all("/\nCREATE TABLE(.*?)\`(.*?)\`/si", "\n".file_get_contents('assets/db/'.$sql_file), $target_tables); 
        foreach ($target_tables[2] as $table) {$mysqli->query('DROP TABLE IF EXISTS '.$table);}
        
        $mysqli->query('SET foreign_key_checks = 1');
        $mysqli->query("SET NAMES 'utf8'"); 
        $templine = ''; // Temporary variable, used to store current query
        
        if($allLines)
        {
            foreach ($allLines as $line){ // Loop through each line
            if (substr($line, 0, 2) != '--' && $line != '') { // Skip it if it's a comment
                $templine .= $line; // Add this line to the current segment
                if (substr(trim($line), -1, 1) == ';') {// If it has a semicolon at the end, it's the end of the query
                    $mysqli->query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . $mysqli->error . '<br /><br />');
                    $templine = '';// Reset temp variable to empty
                }
             }
            }

            $data['class'] = 'alert alert-success';
            $data['message'] = 'You have imported the database successfully.';

        }
        
        $this->load->view($this->layout, $data);
    }

}
