<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends MY_Manager
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('student_model');
		$this->load->model('faculty_model');
	}

	public function index()
	{
		$data = array(
	        'title' => 'List Students',
	        'view' => 'manager/students/list',
	        'students' => $this->student_model->getRows(),
            'faculty' => $this->faculty_model
        );

		$this->load->view($this->layout, $data);
	}

	public function create()
	{
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('studentID', 'student ID', 'required|integer');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('userpass', 'userpass', 'required');
		$this->form_validation->set_rules('facultyID', 'facultyID', 'required|integer');

		$data = array(
	        'title' => 'Create a student',
	        'view' => 'manager/students/create',
	        'faculties' => $this->faculty_model->getRows(),
        );

        $student = array(
        	'username' => $this->input->post('username'),
        	'userpass' => md5($this->input->post('userpass')),
        	'facultyID' => $this->input->post('facultyID'),
            'name' => $this->input->post('name'),
            'studentID' => $this->input->post('studentID')
        );

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->student_model->save($student);

                $data['message'] = 'The student has been added successfully.';
                $data['class']   = 'alert alert-success';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view($this->layout, $data);
	}

	public function edit($id)
	{
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('studentID', 'student ID', 'required|integer');
		$this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('facultyID', 'facultyID', 'required|integer');

		$data = array(
	        'title' => 'Edit a student',
	        'view' => 'manager/students/edit',
	        'faculties' => $this->faculty_model->getRows(),
	        'data' => $this->student_model->getRow($id)
        );

        $student = array(
        	'id' => $id,
        	'username' => $this->input->post('username'),
            'userpass' => md5($this->input->post('userpass')),
            'facultyID' => $this->input->post('facultyID'),
            'name' => $this->input->post('name'),
            'studentID' => $this->input->post('studentID')
        );

        if($student['userpass'] == 'd41d8cd98f00b204e9800998ecf8427e')
        {
            $student['userpass'] = $data['data']['userpass'];
        } 

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->student_model->update($student);

                $data['message'] = 'The student has been edited successfully.';
                $data['class']   = 'alert alert-success';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view($this->layout, $data);
	}

	public function delete($id)
	{
		$data = array(
	        'title' => 'Delete a student',
	        'view' => 'manager/students/delete',
        );

		if($this->input->post())
        {
            $this->student_model->delete($id);

            $data['message'] = 'The student has been deleted successfully.';
            $data['class']   = 'alert alert-success';
            
            redirect('manager/students');
        }

        $this->load->view($this->layout, $data);
	}
}
