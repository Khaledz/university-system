<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Manager
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('note_model');
        $this->load->model('class_model');
        $this->load->model('event_model');
    }

    public function index()
    {
        $timestamp = strtotime(date('Y-m-d'));

        $day = date('l', $timestamp);

        $data = array(
            'title' => 'Dashboard',
            'view' => 'manager/dashboard/index',
            'notes' => $this->note_model->getRowsByManagerID($this->managerID),
            'classes' => $this->class_model->getClassesByDay($day),
            'events' => $this->event_model->getEventsByManagerID($this->managerID)
        );

        $this->load->view($this->layout, $data);
    }
}
