<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends MY_Manager
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('class_model');
		$this->load->model('faculty_model');
		$this->load->model('subject_model');
        $this->load->model('lecturer_model');
	}

	public function index()
	{
		$data = array(
	        'title' => 'List Classes',
	        'view' => 'manager/classes/list',
	        'classes' => $this->class_model->listClasses()
        );

		$this->load->view($this->layout, $data);
	}

	public function create()
	{
		$this->form_validation->set_rules('day', 'day', 'required');
		$this->form_validation->set_rules('facultyID', 'faculty', 'required|integer');
		$this->form_validation->set_rules('subjectID', 'subject', 'required|integer');

		$data = array(
	        'title' => 'Create a class',
	        'view' => 'manager/classes/create',
	        'faculties' => $this->faculty_model->getRows(),
	        'subjects' => $this->subject_model->getRows(),
            'lecturers' => $this->lecturer_model->getRows()
        );

        $class = array(
        	'starttime' => $this->input->post('starttime'),
        	'endtime' => $this->input->post('endtime'),
        	'day' => $this->input->post('day'),
        	'subjectID' => $this->input->post('subjectID'),
        	'facultyID' => $this->input->post('facultyID'),
            'lecturerID' => $this->input->post('lecturerID')
        );

        $class['hours'] = $class['endtime'] - $class['starttime'];

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->class_model->save($class);

                $data['message'] = 'The class has been added successfully.';
                $data['class']   = 'alert alert-success';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view($this->layout, $data);
	}

	public function edit($id)
	{
		$this->form_validation->set_rules('starttime', 'start time', 'required');
		$this->form_validation->set_rules('endtime', 'end time', 'required');
		$this->form_validation->set_rules('day', 'day', 'required');
		$this->form_validation->set_rules('facultyID', 'faculty', 'required|integer');
		$this->form_validation->set_rules('subjectID', 'subject', 'required|integer');

		$data = array(
	        'title' => 'Edit a class',
	        'view' => 'manager/classes/edit',
	        'faculties' => $this->faculty_model->getRows(),
	        'subjects' => $this->subject_model->getRows(),
	        'data' => $this->class_model->getRow($id)
        );

        $class = array(
        	'id' => $id,
        	'starttime' => $this->input->post('starttime'),
        	'endtime' => $this->input->post('endtime'),
        	'day' => $this->input->post('day'),
        	'subjectID' => $this->input->post('subjectID'),
        	'facultyID' => $this->input->post('facultyID'),
            'lecturerID' => $this->input->post('lecturerID')
        );

        $class['hours'] = $class['endtime'] - $class['starttime'];

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->class_model->update($class);

                $data['message'] = 'The class has been edited successfully.';
                $data['class']   = 'alert alert-success';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view($this->layout, $data);
	}

	public function delete($id)
	{
		$data = array(
	        'title' => 'Delete a class',
	        'view' => 'manager/classes/delete',
        );

		if($this->input->post())
        {
            $this->class_model->delete($id);

            $data['message'] = 'The class has been deleted successfully.';
            $data['class']   = 'alert alert-success';
            
            redirect('manager/classes');
        }

        $this->load->view($this->layout, $data);
	}

    public function listLecturersByAjax()
    {
        $facultyID = $this->input->post('facultyID');

        $lecturers = $this->lecturer_model->listLecturersByFacultyID($facultyID);

        if($lecturers)
        {
            foreach($lecturers as $lecturer)
            {
                echo '<option value="'.$lecturer['id'].'">'.$lecturer['name'].'</option>';
            }
        }
        else
        {
            echo '';
        }
        
    }
}
