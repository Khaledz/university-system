<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends MY_Manager
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('event_model');
	}

	public function index()
	{
		$data = array(
	        'title' => 'View Events',
	        'view' => 'manager/events/show',
	        'events' => $this->event_model->getEventsByManagerID($this->managerID),
        );

		$this->load->view($this->layout, $data);
	}

	public function create()
	{
		$this->form_validation->set_rules('event', 'event', 'required');
		$this->form_validation->set_rules('start', 'start', 'required');
        $this->form_validation->set_rules('end', 'end', 'required');

		$data = array(
	        'title' => 'Create an event',
	        'view' => 'manager/events/create',
        );

        $event = array(
        	'title' => $this->input->post('event'),
        	'start' => $this->input->post('start'),
            'end' => $this->input->post('end'),
            'managerID' => $this->managerID
        );

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                $this->event_model->save($event);

                $data['message'] = 'The event has been added successfully.';
                $data['class']   = 'alert alert-success';
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view($this->layout, $data);
	}

    public function getEventsAjax()
    {
        header('Access-Control-Allow-Origin: http://localhost/abdulqader'); 
        $manager = $this->session->userdata('managerdata');

        echo json_encode($this->event_model->getEventsByManagerID($manager['id']));
    }
}
