<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Session extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

        $this->load->model('manager_model');
	}

	public function index(){}

	public function login()
	{
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('userpass', 'userpass', 'required');

		$data = array(
	        'title' => 'Log In',
	        'view' => 'manager/sessions/start',
        );

        $manager = array(
        	'username' => $this->input->post('username'),
        	'userpass' => $this->input->post('userpass')
        );

        if($this->input->post())
        {
            if($this->form_validation->run())
            {
                if($this->manager_model->login($manager['username'], $manager['userpass']))
                {
                    $manager['id'] = $this->manager_model->select('id', 'username', $manager['username']);
                    $this->session->set_userdata('managerdata', $manager);
                    redirect('manager/dashboard');
                }
                else
                {
                    $data['message'] = 'The information is not correct.';
                    $data['class']   = 'alert alert-danger';
                }
            }
            else
            {
                $data['message'] = validation_errors();
                $data['class']   = 'alert alert-danger';
            }
        }

		$this->load->view('manager/sessions/start', $data);
	}

	public function logout()
	{
        $data = array(
            'title' => 'Log Out',
            'view' => 'manager/sessions/logout',
        );

		if($this->session->userdata('managerdata'))
        {
            $this->session->unset_userdata('managerdata');

            $data['message'] = 'You have logged ou successfully.';
            $data['class'] = 'alert alert-success';
        }

		$this->load->view('layouts/manager_layout', $data);
	}
}
