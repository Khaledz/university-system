<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Class_model extends MY_Model 
{
	function __construct()
    {
        parent::__construct();

        $this->table = 'class';
    }

    function listClasses()
    {
    	$query = $this->db->query('SELECT class.*, 
    	subject.name as subjectName,
    	subject.code as subjectCode,
    	faculty.name as facultyName FROM class
    	INNER JOIN subject ON subject.id = class.subjectID
    	INNER JOIN faculty on faculty.id = class.facultyID');

    	if($query->num_rows() > 0)
			return $query->result_array();
		else
			return false;
    }

    function getClassesByDay($day)
    {
        $query = $this->db->query('SELECT class.*, 
        subject.name as subjectName,
        subject.code as subjectCode,
        faculty.name as facultyName FROM class
        INNER JOIN subject ON subject.id = class.subjectID
        INNER JOIN faculty on faculty.id = class.facultyID
        WHERE class.day = "'.$day.'"');

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return false;
    }

    function getClassesByLectrerID($lecturerID)
    {
        $query = $this->db->query('SELECT class.*, 
        subject.name as subjectName,
        subject.code as subjectCode,
        faculty.name as facultyName FROM class
        INNER JOIN subject ON subject.id = class.subjectID
        INNER JOIN faculty on faculty.id = class.facultyID
        WHERE lecturerID = '.$lecturerID.'');

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return false;
    }

    function getClassesByLecturerSession($lecturerID, $day)
    {
        $query = $this->db->query('SELECT class.*, 
        subject.name as subjectName,
        subject.code as subjectCode,
        faculty.name as facultyName FROM class
        INNER JOIN subject ON subject.id = class.subjectID
        INNER JOIN faculty on faculty.id = class.facultyID
        WHERE lecturerID = '.$lecturerID.'
        AND class.day = "'.$day.'"');

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return false;
    }

    function getClassesByStudentSession($studentID, $day)
    {
        $query = $this->db->query('SELECT class.*, 
        subject.name as subjectName,
        subject.code as subjectCode,
        faculty.name as facultyName FROM class
        INNER JOIN subject ON subject.id = class.subjectID
        INNER JOIN faculty on faculty.id = class.facultyID
        INNER JOIN attendence on attendence.classID = class.id
        WHERE attendence.studentID = '.$studentID.'
        AND class.day = "'.$day.'"');

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return false;
    }
}