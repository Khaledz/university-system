<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendence_model extends MY_Model 
{
	function __construct()
    {
        parent::__construct();

        $this->table = 'attendence';
    }

    public function getStudentsByClassID($classID)
    {
    	$this->db->where('classID', $classID);
    	$query = $this->db->get($this->table);

    	if($query->num_rows() > 0)
    	{
    		return $query->result_array();
    	}
    	return false;
    }

    public function update($data)
    {
        $this->db->where('studentID', $data['studentID']);
        $this->db->where('classID', $data['classID']);

        $this->db->update($this->table, $data); 
            return $this->db->affected_rows();
        return false;
    }

    public function multiSelect($target, $where1, $value1, $where2, $value2)
    {
        $query = $this->db->query("SELECT {$target} FROM {$this->table} WHERE {$where1} = '{$value1}' AND {$where2} = '{$value2}'");
        if($query->num_rows() > 0)
            return $query->row()->{$target};
        else
            return false;
    }

    public function getAllStudentsByClassID($classID)
    {
        $query = $this->db->query('SELECT * FROM attendence INNER JOIN student
        ON attendence.studentID = student.id
        WHERE attendence.classID = '.$classID.'');

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return false;
    }

    public function getStudentInformation($studentID)
    {
        $query = $this->db->query('SELECT * FROM attendence INNER JOIN student
        ON attendence.studentID = student.id
        WHERE attendence.studentID = '.$studentID.'');

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return false;
    }

}