<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Note_model extends MY_Model 
{
	function __construct()
    {
        parent::__construct();

        $this->table = 'note';
    }

    function getRowsByManagerID($managerID)
    {
    	$this->db->where('managerID', $managerID);
    	$query = $this->db->get($this->table);

    	if($query->num_rows() > 0)
    	{
    		return $query->result_array();
    	}
    	return false;
    }

    function getRowsByLecturerID($lecturerID)
    {
        $this->db->where('lecturerID', $lecturerID);
        $query = $this->db->get($this->table);

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return false;
    }

    function getRowsByStudentID($studentID)
    {
        $this->db->where('studentID', $studentID);
        $query = $this->db->get($this->table);

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return false;
    }
}