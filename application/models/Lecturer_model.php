<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lecturer_model extends MY_Model 
{
	function __construct()
    {
        parent::__construct();

        $this->table = 'lecturer';
    }

    function login($username, $userpass)
	{
		$query = $this->db->get_where($this->table, array('username' => $username, 'userpass' => md5($userpass)));
		if($query->num_rows() > 0)
			return true;
		else
			return false;
	}

	function listLecturersByFacultyID($facultyID)
	{
		$this->db->where('facultyID', $facultyID);
		$query = $this->db->get($this->table);

		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		return false;
	}
}